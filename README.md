## Neurablock bytecode ML detector

The neurablock bot works by analyzing opcodes and bytecode which emit a value between 0 and 1. If the model obtains a value equal to or greater than 0.5, it means that the contract is malicious and therefore will issue an alert.
The analysis is based on a combination of different models created by neurablock using different algorithms and joining them through a stack.

### Alert generated

In the event that an alert is generated regarding your contract, we recommend activating the security teams to check that the transactions that are taking place from that contract are not malicious and, if so, pause the operation or activate the response systems. incident.

### For technical questions or support

Please contact us at hello@neurablock.ai
